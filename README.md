Dungeon_Depths
============== 

*If you are not an adult (18+), please do not play this game or go through this repository.*

This is the official git repo for Dungeon_Depths (D_D), a 2D rogue-like dungeon crawler featuring adult transformation and hypnosis themes.    The game runs on a custom engine written in VB.net/Windows Forms. 

**(This guide is very incomplete at this time)**

Basic Process of Contributing:
-------

When sending in a pull request, please describe what you have done and what name you would like to be credited as (Credit will be given for your work in the "special thanks" portion of the game's about page).

---
©2016-2022
using Assets.Scripts;
using Scripts;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class CombatModal : Modal, HoverMenu.IHoverMenuChoiceHandler, ICombatModal
{
    private static GameObject prefab_ally_entity_panel;
    private static GameObject prefab_enemy_entity_panel;


    private IBattleInputHandler battle_input_handler;
    private IPlayerHealthMaster player_health_master;
    

    private UIRectangle turn_bar;
    private UIRectangle turn_bar_base;
    private TextMeshProUGUI turn_bar_text;
    
    private List<IPlayable> allies;
    private List<ICombatant> enemies;
    private Transform ally_container;
    private Transform enemy_container;

    [SerializeField]
    private ICombatant awaiting_input_from;

    private bool selecting_target;
    private bool attacked;
    private Ability used_ability;

    #region Buttons
    private Transform attack_btn_transform;
    private Button attack_btn;
    private Transform magic_dropdown_transform;
    private Button magic_btn;
    private HoverMenu magic_hover_menu;
    private Transform special_dropdown_transform;
    private Button special_btn;
    private HoverMenu special_hover_menu;
    private Transform use_item_btn_transform;
    private Button use_item_btn;
    private Transform wait_btn_transform;
    private Button wait_btn;
    private Transform run_btn_transform;
    private Button run_btn;
    #endregion

    private TMP_InputField battle_information_text;
    private Scrollbar battle_information_scrollbar;

    private List<CombatantPanel> combatant_panels;
    
    protected override void ModalStart()
    {
        Transform turn_container = panel.Find("Turn Container");
        turn_bar = turn_container.Find("Turn Bar").GetComponent<UIRectangle>();
        turn_bar_base = turn_container.Find("Turn Bar Base").GetComponent<UIRectangle>();
        turn_bar_text = turn_container.Find("Text").GetComponent<TextMeshProUGUI>();

        ally_container = panel.Find("Ally Container");
        enemy_container = panel.Find("Enemy Container");

        Transform btnRoot = panel.Find("Buttons");
        attack_btn_transform = btnRoot.Find("Attack Button");
        attack_btn = attack_btn_transform.GetComponent<Button>();
        magic_dropdown_transform = btnRoot.Find("Magic Dropdown");
        magic_btn = magic_dropdown_transform.GetComponent<Button>();
        magic_hover_menu = magic_dropdown_transform.GetComponent<HoverMenu>();
        special_dropdown_transform = btnRoot.Find("Special Dropdown");
        special_btn = special_dropdown_transform.GetComponent<Button>();
        special_hover_menu = special_dropdown_transform.GetComponent<HoverMenu>();
        use_item_btn_transform = btnRoot.Find("Use Item Button");
        use_item_btn = use_item_btn_transform.GetComponent<Button>();
        wait_btn_transform = btnRoot.Find("Wait Button");
        wait_btn = wait_btn_transform.GetComponent<Button>();
        run_btn_transform = btnRoot.Find("Run Button");
        run_btn = run_btn_transform.GetComponent<Button>();

        GameObject battle_info_text_container = panel.Find("Text Area").Find("TextContainer").gameObject;
        battle_information_text = battle_info_text_container.transform.Find("InputField").GetComponent<TMP_InputField>();
        battle_information_scrollbar = battle_info_text_container.transform.Find("Scrollbar").GetComponent<Scrollbar>();

        if(prefab_ally_entity_panel == null)
        {
            prefab_ally_entity_panel = Resources.Load<GameObject>("Prefabs" + SEP + "UI" + SEP + "Elements" + SEP + "Friendly Entity Panel");
        }
        if(prefab_enemy_entity_panel == null)
        {
            prefab_enemy_entity_panel = Resources.Load<GameObject>("Prefabs" + SEP + "UI" + SEP + "Elements" + SEP + "Enemy Entity Panel");
        }

        //TODO Do these need to be changed to OnSubmit?
        attack_btn.onClick.AddListener(delegate { on_attack_button_clicked(); });
        run_btn.onClick.AddListener(delegate { on_run_button_clicked(); });
        wait_btn.onClick.AddListener(delegate { on_wait_button_clicked(); });

        selecting_target = false;

        combatant_panels = new List<CombatantPanel>();
    }

    protected override void SetDefault()
    {
        if (selecting_target)
        {
            if(attacked)
            {
                //Attacking defaults to enemy
                Player.event_system.SetSelectedGameObject(getCombatantPanel(false, 0).gameObject);
            }
            else if(used_ability != null)
            {
                //Ability default depends on ability
                if(used_ability.default_target == Ability.TARGET_TYPE.ENEMY)
                {
                    Player.event_system.SetSelectedGameObject(getCombatantPanel(false, 0).gameObject);
                }
                else if(used_ability.default_target == Ability.TARGET_TYPE.SELF)
                {
                    Player.event_system.SetSelectedGameObject(get_combatant_panel(awaiting_input_from).gameObject);
                }
                else if(used_ability.default_target == Ability.TARGET_TYPE.ALLY)
                {
                    Player.event_system.SetSelectedGameObject(getCombatantPanel(true, 0).gameObject);
                }
            }
            //CustomEventSystem.instance.SetResetSelection(enemy_container.GetChild(0).GetComponent<Selectable>());
        }
        else if(awaiting_input_from != null)
        {
            Player.event_system.SetSelectedGameObject(attack_btn.gameObject);
        }
        else
        {
            Player.event_system.SetSelectedGameObject(null);
        }
    }

    void Start()
    {
        player_health_master = Master.get_master<IPlayerHealthMaster>();
        battle_input_handler = Master.get_master<IBattleInputHandler>();

        Player.input.UI.Cancel.performed += ctx => cancel_selection();
    }
    
    public void init(List<IPlayable> allies, List<ICombatant> enemies)
    {
        this.allies = allies;
        foreach (IPlayable combatant in allies)
        {
            GameObject go = Instantiate(prefab_ally_entity_panel, ally_container);
            CombatantPanel cp = go.GetComponent<CombatantPanel>();
            go.GetComponent<Button>().onClick.AddListener(() => { on_combatant_clicked(combatant); });
            cp.init(combatant);
            combatant_panels.Add(cp);
        }

        this.enemies = enemies;
        foreach (ICombatant combatant in enemies)
        {
            GameObject go = Instantiate(prefab_enemy_entity_panel, enemy_container);
            CombatantPanel cp = go.GetComponent<CombatantPanel>();
            go.GetComponent<Button>().onClick.AddListener(() => { on_combatant_clicked(combatant); });
            cp.init(combatant);
            combatant_panels.Add(cp);
        }

        set_default_combatant_nav_targets();
    }
    
    #region ATB
    public void updateATBs(Dictionary<ICombatant, float> amounts)
    {
        foreach(KeyValuePair<ICombatant, float> pair in amounts)
        {
            foreach(CombatantPanel panel in combatant_panels)
            {
                if(pair.Key.Equals(panel.combatant))
                {
                    panel.set_ATB_amount(pair.Value);
                    break;
                }
            }
        }
    }
    
    public void update_single_ATB(ICombatant combatant, float amount)
    {
        get_combatant_panel(combatant).set_ATB_amount(amount);
    }
    #endregion

    public void set_combatant_turn(ICombatant current_combatant)
    {
        set_acting(current_combatant);

        awaiting_input_from = current_combatant;
        //This will update the default which will trigger the selecting
        SetDefault();
        
        if(awaiting_input_from != null)
        {
            set_combatants_nav_targets(true);
            
            magic_hover_menu.init(this, ((IPlayable)current_combatant).getSpells().ToArray(), true);
            special_hover_menu.init(this, ((IPlayable)current_combatant).getSpecials().ToArray(), true);
        }
        else
        {
            set_combatants_nav_targets(false);

            magic_hover_menu.set_can_open(false);
            special_hover_menu.set_can_open(false);
        }
    }

    public void update_combatant_bars(ICombatant to_update)
    {
        CombatantPanel combatant_panel = get_combatant_panel(to_update);
        if (combatant_panel != null) { combatant_panel.update_stat_bars(); }
        if(to_update is Player)
        {
            player_health_master.update_all_bars();
        }
        //If panel == null, it's probably because the combatant died
    }

    #region Messages
    public void set_message(string message)
    {
        battle_information_text.text = message;
        battle_information_text.GetComponent<SizeUpdate>().update_size();
        battle_information_scrollbar.value = 1; //Set scrollbar to top
    }

    public void display_message(string message)
    {
        set_message(battle_information_text.text + "\n" + message);
    }
    #endregion

    public void update_player_bars()
    {
        combatant_panels.Where(cp => cp.combatant is Player).ToList().ForEach(cp => cp.update_stat_bars());
    }

    #region Clicked
    private void on_attack_button_clicked()
    {
        if(awaiting_input_from != null)
        {
            attacked = true;
            used_ability = null;

            selecting_target = true;

            set_combatants_nav_targets(false);

            SetDefault();
        }
        else
        {
            //CustomEventSystem.instance.SelectDefault(); //TODO ???
        }
    }

    public void on_hover_menu_choice_clicked(Ability choice)
    {
        if (awaiting_input_from != null)
        {
            attacked = false;
            used_ability = choice;

            selecting_target = true;

            if (choice.possible_targets == Ability.TARGET_TYPE.SELF)
            {
                //Can only target self
                on_combatant_clicked(awaiting_input_from);
            }
            else
            {
                set_combatants_nav_targets(choice.possible_targets);

                //Selects based on ability default_choice
                SetDefault();
            }
        }
        else
        {
            //CustomEventSystem.instance.SelectDefault(); //TODO ???
        }
    }

    public void on_run_button_clicked()
    {
        if(awaiting_input_from != null)
        {
            bool success = battle_input_handler.on_battle_input_run();
            if(success) { return; } //Battle has ended, don't bother with anything else

        }
    }

    private void on_combatant_clicked(ICombatant clicked_combatant)
    {
        if (awaiting_input_from != null && selecting_target)
        {
            selecting_target = false;

            if(attacked) { battle_input_handler.on_battle_input_attack(clicked_combatant); }
            else if(used_ability != null) { battle_input_handler.onBattleInputAbility(clicked_combatant, used_ability); }

            set_combatants_nav_targets(true);
            
            SetDefault();
        }
        else
        {
            //CustomEventSystem.instance.SelectDefault(); //TODO ???
        }
    }

    private void on_wait_button_clicked()
    {
        battle_input_handler.on_battle_input_wait();
    }
    #endregion

    private void cancel_selection()
    {
        if(selecting_target)
        {
            selecting_target = false;
            set_default_combatant_nav_targets();
            if(used_ability != null)
            {
                if(used_ability is Spell)
                {
                    Player.event_system.SetSelectedGameObject(magic_btn.gameObject);
                }
                else if(used_ability is Special)
                {
                    Player.event_system.SetSelectedGameObject(special_btn.gameObject);
                }
            }
            else
            {
                Player.event_system.SetSelectedGameObject(attack_btn.gameObject);
            }
        }
    }

    #region Nav
    private void set_default_combatant_nav_targets()
    {
        Selectable atk_btn_selectable = attack_btn.GetComponent<Selectable>();
        Navigation temp;
        Selectable previous = null;
        foreach (ICombatant combatant in allies)
        {
            CombatantPanel cp = get_combatant_panel(combatant);

            Selectable thisSelectable = cp.selectable;
            temp = cp.navigation;
            temp.selectOnRight = atk_btn_selectable;
            cp.navigation = temp;

            if (previous != null)
            {
                temp.selectOnUp = previous;
                cp.navigation = temp;

                temp = previous.navigation;
                temp.selectOnDown = thisSelectable;
                previous.navigation = temp;
            }

            previous = thisSelectable;
        }

        //Set navigation for left-most buttons to go to lowest (and thus closest) ally
        if (ally_container.childCount > 0)
        {
            Selectable last = ally_container.GetChild(ally_container.childCount - 1).GetComponent<Selectable>();

            temp = attack_btn.navigation;
            temp.selectOnLeft = last;
            attack_btn.navigation = temp;

            temp = use_item_btn.navigation;
            temp.selectOnLeft = last;
            use_item_btn.navigation = temp;
        }


        Selectable special_btn_selectable = special_btn.GetComponent<Selectable>();
        previous = null;
        foreach (ICombatant combatant in enemies)
        {
            CombatantPanel cp = get_combatant_panel(combatant);

            Selectable thisSelectable = cp.selectable;
            temp = cp.navigation;
            temp.selectOnLeft = special_btn_selectable;
            cp.navigation = temp;
            
            if (previous != null)
            {
                temp.selectOnUp = previous;
                cp.navigation = temp;

                temp = previous.navigation;
                temp.selectOnDown = thisSelectable;
                previous.navigation = temp;
            }

            cp.update_stat_bars();

            previous = thisSelectable;
        }

        //Set navigation for right-most buttons to go to lowest (and thus closest) enemy
        if (enemy_container.childCount > 0)
        {
            Selectable last = enemy_container.GetChild(enemy_container.childCount - 1).GetComponent<Selectable>();

            temp = special_btn.navigation;
            temp.selectOnRight = last;
            special_btn.navigation = temp;

            temp = run_btn.navigation;
            temp.selectOnRight = last;
            run_btn.navigation = temp;
        }
    }
    
    private void set_combatants_nav_targets(Ability.TARGET_TYPE possible)
    {
        if(possible == Ability.TARGET_TYPE.ALL)
        {
            //All, so just go with default, minus allowing them to go back to the buttons
            set_combatants_nav_targets(false);
        }
        else if(possible == Ability.TARGET_TYPE.SELF)
        {
            get_combatant_panel(awaiting_input_from).selectable.Select();
            Debug.LogError("setCombatantsNavTargets() for type SELF, which should be handled in onHoverMenuChoiceClicked");
        }
        else if(possible == Ability.TARGET_TYPE.ALLY)
        {
            foreach(ICombatant ally in allies)
            {
                CombatantPanel cp = get_combatant_panel(ally);
                Navigation nav = cp.navigation;
                nav.selectOnRight = null;
                cp.navigation = nav;
            }
        }
        else if (possible == Ability.TARGET_TYPE.ENEMY)
        {
            foreach (ICombatant enemy in enemies)
            {
                CombatantPanel cp = get_combatant_panel(enemy);
                Navigation nav = cp.navigation;
                nav.selectOnLeft = null;
                cp.navigation = nav;
            }
        }
    }

    private void set_combatants_nav_targets(bool to_buttons)
    {
        Selectable allyTarget = null;
        Selectable enemyTarget = null;

        if (to_buttons)
        {
            allyTarget = attack_btn.GetComponent<Selectable>();
            enemyTarget = special_btn.GetComponent<Selectable>();
        }

        Button btn;
        Navigation temp;
        for(int i = 0; i < ally_container.childCount; i++)
        {
            btn = ally_container.GetChild(i).GetComponent<Button>();
            temp = btn.navigation;
            if (to_buttons) { temp.selectOnRight = allyTarget; }
            else
            {
                Selectable paired = enemy_container.GetChild(getPairedIndex(i, ally_container.childCount, enemy_container.childCount)).GetComponent<Selectable>();
                temp.selectOnRight = paired;
            }
            btn.navigation = temp;
        }
        
        for(int i = 0; i < enemy_container.childCount; i++)
        {
            btn = enemy_container.GetChild(i).GetComponent<Button>();
            temp = btn.navigation;
            if (to_buttons) { temp.selectOnLeft = enemyTarget; }
            else
            {
                Selectable paired = ally_container.GetChild(getPairedIndex(i, enemy_container.childCount, ally_container.childCount)).GetComponent<Selectable>();
                temp.selectOnLeft = paired;
            }
            btn.navigation = temp;
        }
    }
    #endregion

    public void remove_combatant(ICombatant combatant)
    {
        CombatantPanel panel = get_combatant_panel(combatant);
        combatant_panels.Remove(panel);

        if(is_ally_to_player(combatant))
        {
            //No need to remove. BattleMaster should handle this, 
            //as the list is passed by reference and it should have already removed the ally from its list
            //allies.Remove(combatant); 
            if (attack_btn.navigation.selectOnLeft == panel.selectable || 
                use_item_btn.navigation.selectOnLeft == panel.selectable)
            {
                if (allies.Count > 0)
                {
                    ICombatant newLast = allies[allies.Count - 1];
                    CombatantPanel newLastPanel = get_combatant_panel(newLast);

                    Navigation temp;

                    temp = attack_btn.navigation;
                    temp.selectOnLeft = newLastPanel.selectable;
                    attack_btn.navigation = temp;

                    temp = use_item_btn.navigation;
                    temp.selectOnLeft = newLastPanel.selectable;
                    use_item_btn.navigation = temp;
                }
            }
        }
        else
        {
            //No need to remove. BattleMaster should handle this, 
            //as the list is passed by reference and it should have already removed the enemy from its list
            //enemies.Remove(combatant); 
            if (special_btn.navigation.selectOnRight == panel.selectable ||
                run_btn.navigation.selectOnRight == panel.selectable)
            {
                if(enemies.Count > 0)
                {
                    ICombatant newLast = enemies[enemies.Count - 1];
                    CombatantPanel newLastPanel = get_combatant_panel(newLast);

                    Navigation temp;

                    temp = special_btn.navigation;
                    temp.selectOnRight = newLastPanel.selectable;
                    special_btn.navigation = temp;

                    temp = run_btn.navigation;
                    temp.selectOnRight = newLastPanel.selectable;
                    run_btn.navigation = temp;
                }
            }
        }

        Destroy(panel.gameObject);
        //foreach(Transform t in enemy_container.transform) //Iterates through children
        //{
        //    CombatantPanel cp = t.GetComponent<CombatantPanel>();
        //    if(cp != null && cp.myCombatant.Equals(combatant))
        //    {
        //        Destroy(t.gameObject);
        //        break;
        //    }
        //}
    }
    
    public void set_acting(ICombatant active)
    {
        if(awaiting_input_from != null)
        {
            get_combatant_panel(awaiting_input_from).set_acting(false);
        }
        if (active != null)
        {
            get_combatant_panel(active).set_acting(true);
        }
    }

    public void set_turn_percent(float percent)
    {
        turn_bar.width = turn_bar_base.width * percent;
        turn_bar.SetVerticesDirty();
    }

    public void set_turn_text(string turn_text)
    {
        turn_bar_text.text = turn_text;
    }

    #region Util
    public bool is_ally_to_player(ICombatant combatant)
    {
        if(combatant is IPlayable)
        {
            if(allies.Contains((IPlayable)combatant)) { return true; }
        }
        if (enemies.Contains(combatant)) { return false; }
        return false;
    }

    private CombatantPanel get_combatant_panel(ICombatant combatant)
    {
        CombatantPanel ret = combatant_panels.Find(cp => cp.combatant.Equals(combatant));
        return ret;
    }

    private CombatantPanel getCombatantPanel(bool ally, int index)
    {
        if(ally)
        {
            return ally_container.GetChild(index).GetComponent<CombatantPanel>();
        }
        else
        {
            return enemy_container.GetChild(index).GetComponent<CombatantPanel>();
        }
    }

    private void purge()
    {
        purge_combatants();
        set_message("");
    }

    private void purge_combatants()
    {
        allies = null;
        enemies = null;
        foreach(Transform t in ally_container)
        {
            Destroy(t.gameObject);
        }
        foreach (Transform t in enemy_container)
        {
            Destroy(t.gameObject);
        }
    }

    private int getPairedIndex(int index, int thisCount, int pairCount)
    {
        //float position;
        //float inc;
        //float fix;
        //if(thisCount == 1) { inc = 0f; fix = 0.5f; }
        //else if(thisCount == 2) { inc = 0.25f; fix = 0.25f; }
        //else if(thisCount == 3) { inc = 0.33f; fix = 0f; }
        //else if(thisCount == 4) { inc = 0.25f; fix = 0f; }
        
        if (pairCount == 1) { return 0; }

        if (thisCount == 1)
        {
            if (pairCount <= 2) { return 0; }
            else { return 1; }
        }
        else if (thisCount == 2)
        {
            if (index == 0)
            {
                if (pairCount <= 3) { return 0; }
                else { return 1; }
            }
            else
            {
                if (pairCount <= 2) { return 1; }
                else { return 2; }
            }
        }
        else if (thisCount == 3)
        {
            if (index == 0) { return 0; }
            else if (index == 1)
            {
                if (pairCount <= 2) { return 0; }
                else { return 1; }
            }
            else
            {
                if (pairCount <= 2) { return 1; }
                else { return 2; }
            }
        }
        else// if (thisCount == 4)
        {
            if (index == 0 || pairCount == 0) { return 0; }
            else if (index == 1)
            {
                if(pairCount <= 3) { return 0; }
                else { return 1; }
            }
            else if (index == 2)
            {
                if(pairCount == 1) { return 0; }
                else if(pairCount == 2) { return 1; }
                else if(pairCount == 3) { return 1; }
                else { return 2; }
            }
            else
            {
                return pairCount - 1;
            }
        }
    }
    #endregion
}
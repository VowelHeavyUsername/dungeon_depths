﻿public interface IEquipmentMaster
{
    void equip_armor(int id);
    void equip_armor(int id, bool unequip_if_same);
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IEnsureVisible<T>
{
    void ensure_visible(T me);
}

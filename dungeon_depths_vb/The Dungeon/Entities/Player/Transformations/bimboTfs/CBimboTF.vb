﻿Public NotInheritable Class CBimboTF
    Inherits BimboTF
    Public Shared bimbored1 As Color = Color.FromArgb(255, 255, 79, 89)
    Public Shared bimbored2 As Color = Color.FromArgb(255, 255, 38, 49)

    Private Const TF_IND As tfind = tfind.cherrybimbo

    Sub New(n As Integer, tts As Integer, wi As Double, cbs As Boolean)
        MyBase.New(n, tts, wi, cbs)
        MyBase.update_during_combat = False
        tf_name = TF_IND
        next_step = AddressOf hairColorShift
    End Sub
    Sub New(cs As Integer, n As Integer, tts As Integer, wi As Double, cbs As Boolean, tfd As Boolean)
        MyBase.New(cs, n, tts, wi, cbs, tfd)
        MyBase.update_during_combat = False
        tf_name = TF_IND
        next_step = getNextStep(cs)
    End Sub

    'Hair Color Shift
    Overrides Sub hairColorShift()
        Game.player1.prt.haircolor = DDUtils.cShift(Game.player1.prt.haircolor, bimbored1, 25)
        If Not Game.player1.getHairColor.Equals(bimbored1) Then curr_step -= 1
        TextEvent.push("Your hair becomes slightly lighter, brightening to a cherry red.")
    End Sub

    'Step 1
    Public Overrides Sub s1BimboHairChange(ByRef p As Player)
        p.prt.haircolor = bimbored1
        p.prt.setIAInd(pInd.rearhair, 5, True, True)
        p.prt.setIAInd(pInd.midhair, 5, True, True)
        p.prt.setIAInd(pInd.fronthair, 6, True, True)
    End Sub

    'Step 2
    Public Overrides Sub s2M2F(ByRef p As Player, ByRef out As String, ByRef haircolor As String)
        MyBase.s2M2F(p, out, "bright red")
    End Sub
    Public Overrides Sub s2HairChange(ByRef p As Player)
        p.prt.haircolor = bimbored2
        p.prt.setIAInd(pInd.rearhair, 18, True, True)  'rearhair1
        p.prt.setIAInd(pInd.midhair, 22, True, True)  'rearhair2
        p.prt.setIAInd(pInd.fronthair, 19, True, True) 'fronthair
    End Sub
    Public Overrides Sub s2FaceChange(ByRef p As Player)

        p.prt.haircolor = bimbored2
        p.prt.setIAInd(pInd.eyes, 26, True, True)  'eyes

        p.prt.setIAInd(pInd.mouth, 6, True, True)  'mouth
    End Sub

    Public Overrides Function hasBimboHair(p As Player) As Boolean
        Return p.prt.haircolor.Equals(bimbored1) Or p.prt.haircolor.Equals(bimbored2)
    End Function
End Class

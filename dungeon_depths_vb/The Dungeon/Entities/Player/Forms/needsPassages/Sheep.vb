﻿Public Class Sheep
    Inherits pForm
    Sub New()
        MyBase.New(1.5, 0.5, 0.5, 1.5, 0.5, 0.75, "Sheep", False)
        revertPassage = "Your wool begins vanishing in patches as you stand back up on two legs.  When your hooves turn back into hands, you can't help but sigh with relief..."
        canBeTFed = False
    End Sub
End Class

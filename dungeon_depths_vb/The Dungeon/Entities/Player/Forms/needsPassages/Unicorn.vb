﻿Public Class Unicorn
    Inherits pForm
    Sub New()
        MyBase.New(1.35, 0.0, 0.75, 0.85, 1.5, 0.5, "Unicorn", False)
        revertPassage = "Your fur begins vanishing in patches as you stand back up on two legs.  When your hooves turn back into hands, you can't help but sigh with relief..."
        canBeTFed = False
    End Sub
End Class

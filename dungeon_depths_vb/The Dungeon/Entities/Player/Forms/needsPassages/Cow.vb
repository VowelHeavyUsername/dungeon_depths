﻿Public Class Cow
    Inherits pForm
    Sub New()
        MyBase.New(1.5, 0.0, 0.75, 1.35, 0.85, 0.5, "Cow", False)
        revertPassage = "Your fur begins vanishing in patches as you stand back up on two legs.  When your hooves turn back into hands, you can't help but sigh with relief..."
        canBeTFed = False
    End Sub
End Class

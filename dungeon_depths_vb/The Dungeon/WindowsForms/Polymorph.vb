﻿Public Class Polymorph
    Public Shared porm As Boolean = True
    Public target As NPC
    Public tfForm As Boolean = False

    '| - FORM EVENT HANDLERS - |
    Private Sub Polymorph_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'scale to the screen size
        Dim startingWidth = Me.Width
        Dim startingHeight = Me.Height
        If Game.screenSize = "Small" Then
            Size = New Size(Size.Width * 0.8, Size.Height * 0.8)
        ElseIf Game.screenSize = "Medium" Then
            Size = New Size(Size.Width * 0.9, Size.Height * 0.9)
        ElseIf Game.screenSize = "XLarge" Then
            Size = New Size(Size.Width * 1.32, Size.Height * 1.32)
        End If
        Dim RW As Double = (Me.Width - startingWidth) / startingWidth ' Ratio change of width
        Dim RH As Double = (Me.Height - startingHeight) / startingHeight ' Ratio change of height
        Dim newFont As Font = New System.Drawing.Font("Consolas", CInt(8 * Me.Size.Width / 210))
        For i = 0 To Me.Controls.Count - 1
            Me.Controls(i).Font = newFont
            Me.Controls(i).Width += CDbl(Me.Controls(i).Width * RW)
            Me.Controls(i).Height += CDbl(Me.Controls(i).Height * RH)
            Me.Controls(i).Left += CDbl(Me.Controls(i).Left * RW)
            Me.Controls(i).Top += CDbl(Me.Controls(i).Top * RH)
        Next

        Dim p = Game.player1
        Select Case porm
            Case True
                For i = 0 To p.selfPolyForms.Count - 1
                    cboxPolymorph.Items.Add(p.selfPolyForms.Item(i))
                Next
                If cboxPolymorph.Items.Contains(p.className) Then cboxPolymorph.Items.Remove(p.className)
                If cboxPolymorph.Items.Contains(p.formName) Then cboxPolymorph.Items.Remove(p.formName)
            Case False
                For i = 0 To p.enemPolyForms.Count - 1
                    cboxPolymorph.Items.Add(p.enemPolyForms.Item(i))
                Next
        End Select

        Me.CenterToParent()
    End Sub
    Private Sub BtnPolymorphOK_Click(sender As Object, e As EventArgs) Handles btnPolymorphOK.Click
        'Cancel the polymorph if an invalid form type is selected
        If cboxPolymorph.Text = "-- Select --" Or Not tfForm Then
            Me.Close()
            Game.player1.mana += 12
            Exit Sub
        End If

        'Route the polymorph based on target type
        Select Case porm
            Case True
                '| -- Player -- |
                transform(Game.player1, cboxPolymorph.Text)
            Case False
                If target.GetType().IsSubclassOf(GetType(ShopNPC)) Then
                    '| -- NPC -- |
                    transformN(target, cboxPolymorph.Text)
                Else
                    '| -- Enemy -- |
                    transform(target, cboxPolymorph.Text)
                End If
        End Select

        Me.Close()
    End Sub

    '| - PLAYER TRANSFORMATIONS -|
    Shared Sub transform(ByRef p As Player, ByVal form As String, Optional ByVal checkform As Boolean = True)
        '| -- Pre-transformation Checks -- |
        If checkform AndAlso (form.Equals(p.className) Or form.Equals(p.formName) Or Not p.polymorphs.Keys.Contains(form)) Then
            Exit Sub
        End If

        '| -- Revert Previous Form/Class -- |
        Dim revertText = ""
        If Not form.Equals(p.className) Then
            p.pClass.revert()
            revertText = p.pClass.revertPassage & DDUtils.RNRN
        ElseIf Not form.Equals(p.formName) Then
            p.pForm.revert()
            revertText = p.pForm.revertPassage & DDUtils.RNRN
        Else
            DDError.badPolymorphError(form)
        End If

        '| -- Transformation -- |
        p.ongoingTFs.resetPolymorphs()

        Dim active_polymorph = PolymorphTF.newPoly(form)

        p.polymorphs(form) = active_polymorph
        p.ongoingTFs.add(active_polymorph)
        p.perks(perk.polymorphed) = active_polymorph.getTurnsTilNextStep()

        If form = "MASBimbo" Then form = "Bimbo"
        If Player.forms.Keys.Contains(form) Then
            p.changeForm(form)
        ElseIf Player.classes.Keys.Contains(form) Then
            p.changeClass(form)
        End If

        '| -- Cleanup -- |
        If Not revertText = "" & DDUtils.RNRN Then TextEvent.push(revertText & Game.lblEvent.Text.Split(vbCrLf)(0))

        p.ongoingTFs.ping()

        p.specialRoute()
        p.magicRoute()

        p.drawPort()
    End Sub
    'NPC transform method
    Shared Sub transform(ByRef t As NPC, ByVal s As String)
        If s = "Giant Frog" Then                'Even Debuff
            t.maxHealth *= 0.7
            t.attack *= 0.2
            t.defense *= 0.2
            t.speed *= 0.7
            t.will *= 0.2
            t.tfEnd = 5

        ElseIf s = "Sheep" Then                 'Even Debuff
            t.maxHealth *= 0.5
            t.attack *= 0.5
            t.defense *= 0.5
            t.speed *= 0.5
            t.will *= 0.5
            t.tfEnd = 6

        ElseIf s = "Princess" Then              '+WIL Debuff
            t.maxHealth *= 0.33
            t.attack *= 0.33
            t.defense *= 0.33
            t.speed *= 0.33
            t.will *= 1.0
            t.tfEnd = 6

        ElseIf s = "Cat-Girl" Then              '+WIL/SPD Debuff
            t.maxHealth *= 0.33
            t.attack *= 0.33
            t.defense *= 0.33
            t.speed *= 0.33
            t.will *= 1.0
            t.tfEnd = 6

        ElseIf s = "Bunny" Then                 '+SPD Debuff
            t.maxHealth *= 0.33
            t.attack *= 0.33
            t.defense *= 0.33
            t.speed *= 1.0
            t.will *= 0.33
            t.tfEnd = 6

        ElseIf s = "Cow" Then                   '+HP Debuff
            t.maxHealth *= 1.0
            t.attack *= 0.33
            t.defense *= 0.33
            t.speed *= 0.33
            t.will *= 0.33
            t.tfEnd = 6

        ElseIf s = "Trilobite" Then             '+DEF Debuff
            t.maxHealth *= 0.33
            t.attack *= 0.33
            t.defense *= 1.0
            t.speed *= 0.33
            t.will *= 0.33
            t.tfEnd = 6

        ElseIf s = "Amnesiac" Then              'Even Debuff
            t.maxHealth *= 0.33
            t.attack *= 0.33
            t.defense *= 0.33
            t.speed *= 0.33
            t.will *= 0.33
            t.stunct = 1
            t.tfEnd = 3

        ElseIf s = "Slime​" Then                 '+DEF Buff
            t.maxHealth *= 0.5
            t.attack *= 1.2
            t.defense *= 2.5
            t.speed *= 0.9
            t.will *= 1.0
            t.tfEnd = 2

        ElseIf s = "Succubus​" Then              '+ATK/WIL Buff
            t.maxHealth *= 2.0
            t.attack *= 1.5
            t.defense *= 0.75
            t.speed *= 1.5
            t.will *= 1.25
            t.tfEnd = 2

        ElseIf s = "Dragon​" Then                '+HP/ATK/DEF Buff
            t.maxHealth *= 1.5
            t.attack *= 1.5
            t.defense *= 2.0
            t.speed *= 0.5
            t.will *= 1.25
            t.tfEnd = 2

        ElseIf s = "Bee-Girl" Then              '+WIL/SPD Debuff
            t.maxHealth *= 0.5
            t.attack *= 1.5
            t.defense *= 1.5
            t.speed *= 3.0
            t.will *= 0.33
            t.tfEnd = 6
        Else
            Exit Sub
        End If

        t.tfCt = 1
        t.form = s
    End Sub
    'npc transform method
    Shared Sub transformN(ByRef t As ShopNPC, ByVal s As String)
        Polymorph.transform(t, s)

        If s = "Sheep" Then
            t.toSheep()
        ElseIf s = "Princess" Then
            t.toPrincess()
        ElseIf s = "Bunny" Then
            t.toBunny()
        ElseIf s = "Cat-Girl" Then
            t.toCatgirl()
        ElseIf s = "Trilobite" Then
            t.toTrilobite()
        ElseIf s = "Bee-Girl" Then
            t.toBeeGirl()
        End If
    End Sub

    Shared Function rndFName() As String
        Randomize()
        Dim fFNames() As String = {"Abigail", "Abby", "Anna", "Ann", "Ana", "Alexis", "Allie", _
                               "Becky", _
                               "Christine", "Casandra", "Catherine", "Cassie", "Carol", "Caroline", "Cara", _
                               "Danica", _
                               "Ellen", "Erika", "Erica", _
                               "Heather", _
                               "Iliona", _
                               "Janice", "Johanna", "Jenna", "Judy", "Jennifer", "Jo-Jo", _
                               "Kerry", "Katherine", "Katja", _
                               "Lana", _
                               "Monica", "Mary", _
                               "Nancy", "Nicole", "Nadja", _
                               "Racheal", _
                               "Samantha", "Sarah", "Sally", "Sophie", _
                               "Tanja", "Trisha", _
                               "Vanessa"}

        Return fFNames(Int(Rnd() * fFNames.Length))
    End Function
    Shared Sub giveRNDFName(ByRef p As Player)
        p.name = rndFName()
    End Sub
    Shared Function rndMName() As String
        Randomize()
        Dim mFNames() As String = {"Aaron", "Alan", "Alexander", _
                               "Bob", "Bruce", "Brandon", "Bailey", _
                               "Chris", "Ciaran", _
                               "Daniel", "Dave", "David", _
                               "Eric", _
                               "Gerry", _
                               "Hank", _
                               "Issac", "Ian", _
                               "James", "Jimmy", "Jim", "Josh", "John", "Jackson", _
                               "Ken", _
                               "Lorenzo", "Leonard", "Leo", "Lawrence", _
                               "Mark", _
                               "Nathan", "Nick", _
                               "Oliver", _
                               "Richard", "Ryan", _
                               "Samuel", "Stanley", "Stan", "Scott", _
                               "Tanner", "Tristan", "Travis", _
                               "Vance", _
                               "Zachary", "Zack"}

        Return mFNames(Int(Rnd() * mFNames.Length))
    End Function
    Shared Sub giveRNDMName(ByRef p As Player)
        p.name = rndMName()
    End Sub

    Shared Function rndBimName(ByRef p As Player) As String
        Randomize()
        Dim bimNames As Dictionary(Of Char, String()) = New Dictionary(Of Char, String())
        bimNames.Add("A", {"Alicia", "Ana", "Alexis", "Allie", "Amber", "Ali", "Aurora"})
        bimNames.Add("B", {"Becky", "Becki", "Bambi", "Brandi", "Bunni", "Bianca"})
        bimNames.Add("C", {"Crystal", "Coco", "Cassie", "Cara", "Chloe", "Cindi", "Candi"})
        bimNames.Add("D", {"Daisy", "Dani", "Diamond", "Danni", "Dixi", "Daphne"})
        bimNames.Add("E", {"Erika", "Emmy", "Eliza", "Envi", "Evie", "Emily"})
        bimNames.Add("F", {"Felicity", "Frankie", "Faith", "Foxi", "Fia"})
        bimNames.Add("G", {"Gabi", "Gigi", "Glamour", "Gia", "Gia"})
        bimNames.Add("H", {"Heather", "Hailey", "Honey", "Halli", "Haylee", "Harmoni"})
        bimNames.Add("I", {"Izzy", "Izzi", "Ina", "Ivy"})
        bimNames.Add("J", {"Joni", "Jenna", "Jenni", "Jojo", "Jade"})
        bimNames.Add("K", {"Kelli", "Kelsi", "Krystal", "Kitty", "Kyra"})
        bimNames.Add("L", {"Lana", "Leora", "Lexi", "Lace", "Lacy", "Lia", "Lila", "Lori"})
        bimNames.Add("M", {"Monica", "Mia", "Monique", "Merci", "May", "Misty"})
        bimNames.Add("N", {"Nancy", "Nicki", "Nat", "Nica", "Nina"})
        bimNames.Add("O", {"Opal", "Ophelia", "Olivia"})
        bimNames.Add("P", {"Paris", "Pixi", "Porsche", "Pansi"})
        bimNames.Add("Q", {"Quinn", "Qui"})
        bimNames.Add("R", {"Racheal", "Ruby", "Rita", "Ria", "Rio", "Rosi"})
        bimNames.Add("S", {"Sammi", "Sam", "Salli", "Sara", "Sofi", "Staci", "Sapphire", "Skye"})
        bimNames.Add("T", {"Trisha", "Trixie", "Tiffany", "Thia", "Tara", "Tawni", "Tia", "Tricia", "Tess"})
        bimNames.Add("U", {"Unique"})
        bimNames.Add("V", {"Vivi", "Viola", "Venus", "Vanessa"})
        bimNames.Add("W", {"Wendi", "Willow"})
        bimNames.Add("X", {"Xena", "Xia", "Xiola"})
        bimNames.Add("Y", {"Yvonne", "Yvette"})
        bimNames.Add("Z", {"Zena", "Zoe", "Zozo", "Zi"})

        If bimNames.Keys.Contains(p.name.ToUpper.First) Then
            Dim r = Int(Rnd() * bimNames(p.name.ToUpper.First).Length)
            Return bimNames(p.name.ToUpper.First)(r)
        Else
            Dim r1 = Int(Rnd() * bimNames.Keys.Count)
            Dim r2 = Int(Rnd() * bimNames(bimNames.Keys(r1)).Length)
            Return bimNames(bimNames.Keys(r1))(r2)
        End If
    End Function
    Shared Function bimboizeName(ByVal name As String) As String
        If Settings.active(setting.bimbonames) Then Return rndBimName(Game.player1)

        Dim vowels() As String = {"a", "e", "i", "o", "u"}

        Dim oname = name.ToLower
        Dim firstVowel As Integer = oname.Length

        For i = 1 To oname.Length - 1
            If vowels.Contains(oname.Substring(i, 1)) Then
                If Not firstVowel = oname.Length Then firstVowel = i + 1 : Exit For
                firstVowel = i + 1
            End If

        Next
        name = oname.Substring(0, firstVowel)

        If name.EndsWith("ie") Or name.EndsWith("ey") Then
            name = name.Substring(0, name.Length - 1) & "i"
        ElseIf (name.EndsWith("e") Or name.EndsWith("y")) And name.Length > 2 Then
            name = name.Substring(0, name.Length - 1) & "i"
        ElseIf (name.EndsWith("i") Or name.EndsWith("n")) And name.Length > 2 Then
            name = name.Substring(0, name.Length) & "i"
        End If

        name = name.Substring(0, 1).ToUpper() + name.Substring(1, name.Length - 1)

        If (name.EndsWith("chelli")) Then Return name.Substring(0, name.Length - 1) & "e"
        If (name.EndsWith("vi")) Then Return name.Substring(0, name.Length) & "i"
        If (name.StartsWith("Ali")) Then Return "Alli"
        If (name.StartsWith("Mel")) Then Return "Mel"
        If (name.EndsWith("au")) Then Return name.Substring(0, name.Length) & "li"
        If (name.EndsWith("sa") Or name.EndsWith("sta")) Then Return name.Substring(0, name.Length - 2) & "sie"
        If (name.EndsWith("oo")) Then name = name.Substring(0, name.Length - 1)

        If name.Length <= 2 Then
            If (name.EndsWith("Le") Or name.EndsWith("Se") Or name.EndsWith("Ro") Or name.EndsWith("Fo")) Then
                Return name.Substring(0, name.Length - 1) & "xi"
            ElseIf (name.EndsWith("He") Or name.EndsWith("Ka") Or name.EndsWith("Ke") Or name.EndsWith("Ha")) Then
                Return name.Substring(0, 1) & "aylee"
            ElseIf (name.EndsWith("Ju") Or name.EndsWith("Do") Or name.EndsWith("Ca") Or name.EndsWith("Ho")) Then
                Return name.Substring(0, name.Length - 1) & "li"
            ElseIf (name.EndsWith("Co") Or name.EndsWith("Ko")) Then
                Return name.Substring(0, 1) & "hloe"
            ElseIf (name.EndsWith("Ba")) Then
                Return name.Substring(0, name.Length - 1) & "mbi"
            ElseIf (name.EndsWith("Bo")) Then
                Return name.Substring(0, name.Length) & "obi"
            ElseIf (name.EndsWith("Am")) Then
                Return name.Substring(0, name.Length - 1) & "ber"
            ElseIf (name.EndsWith("Se")) Then
                Return name.Substring(0, 1) & "kye"
            ElseIf (name.EndsWith("Lo")) Then
                Return name.Substring(0, 1) & "oona"
            ElseIf (name.EndsWith("Sa")) Then
                Return name.Substring(0, 1) & "tacey"
            ElseIf (name.EndsWith("i") Or name.EndsWith("o") Or name.EndsWith("u")) Then
                Return name & "-" & name
            ElseIf (name.EndsWith("a")) Then
                Return name.Substring(0, name.Length - 1) & "ia"
            ElseIf (name.EndsWith("e")) Then
                Return name.Substring(0, name.Length) & "na"
            End If
        Else
            Return name
        End If

        Return "Allie"
    End Function

    Private Sub cboxPMorph_SelectedValueChanged(sender As Object, e As EventArgs) Handles cboxPolymorph.SelectedValueChanged
        tfForm = True
    End Sub
End Class
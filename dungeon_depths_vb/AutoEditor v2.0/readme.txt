What this does:
-Center and Crop image properly
-Removes skin color
-Removes Background color
-Edits single images
-Edits bulk images loaded into the in/m and in/f folder, outputs to the out folder

What this does not do (future improvements):
-Remove body outline from outside of clothes
